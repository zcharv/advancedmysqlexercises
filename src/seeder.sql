SELECT country.name, MAX(city.Population)
FROM city
INNER JOIN country on city.CountryCode = country.Code
GROUP BY country.name;

SELECT country.name, MIN(city.Population)
FROM city
INNER JOIN country on city.CountryCode = country.Code
GROUP BY country.name;

SELECT country.name, MAX(city.Population), MIN(city.Population), (MAX(city.Population) - MIN(city.Population))
FROM city
INNER JOIN country on city.CountryCode = country.Code
GROUP BY country.name;

SELECT country.name, SUM(city.Population)
FROM city
INNER JOIN country on city.CountryCode = country.Code
GROUP BY country.name;

SELECT country.name, COUNT(LANGUAGE) AS TOTAL
FROM country
INNER JOIN countrylanguage on countrylanguage.CountryCode = country.Code
GROUP BY country.name
ORDER BY TOTAL DESC
LIMIT 5;

SELECT country.name
FROM country
INNER JOIN countrylanguage on countrylanguage.CountryCode = country.Code
WHERE Language = 'Russian'
ORDER BY country.Population DESC
LIMIT 1;

SELECT city.name
FROM city
INNER JOIN country on city.CountryCode = country.Code
INNER JOIN countrylanguage on countrylanguage.CountryCode = country.Code
WHERE Language = 'French' AND country.name != 'France'
ORDER BY city.Population DESC
limit 1;

# SELECT THE TOP TEN SPOKEN LANGUAGES

SELECT country.Region, COUNT(country.Region) AS NUM
FROM country
INNER JOIN countrylanguage on countrylanguage.CountryCode = country.Code
WHERE Language = 'Spanish'
GROUP BY country.Region
ORDER BY NUM DESC;

SELECT (city.Population * countrylanguage.Percentage) AS POP, countrylanguage.Language, countrylanguage.Percentage
FROM city
INNER JOIN countrylanguage on countrylanguage.CountryCode = city.CountryCode
WHERE city.Name = 'New York'
ORDER BY POP DESC;

SELECT city.name, city.Population
FROM city
INNER JOIN country on city.CountryCode = country.Code
WHERE CountryCode = 'USA'
ORDER BY city.Population DESC;

SELECT country.name, country.Population
FROM country
ORDER BY country.Population DESC
LIMIT 1;

SELECT country.Population
FROM country
WHERE SUM(country.Population) < MAX(country.Population)
;

SELECT country.name, COUNT(city.Name) AS TOTAL
FROM country
INNER JOIN city on city.CountryCode = country.Code
GROUP BY country.name
ORDER BY TOTAL DESC
LIMIT 1;

SELECT country.name, COUNT(city.Name) AS TOTAL_CITY, COUNT(country.Name) AS TOTAL_COUNTRY
FROM country
INNER JOIN city on city.CountryCode = country.Code
WHERE TOTAL = T
GROUP BY country.name
ORDER BY TOTAL DESC
LIMIT 1;
